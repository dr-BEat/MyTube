﻿using System;
using System.Net;
using Foundation;
using MyTube.iOS.Services;
using MyTube.Services;
using Xamarin.Forms;

[assembly: Dependency(typeof(CookieManager))]
namespace MyTube.iOS.Services
{
    class CookieManager : ICookieManager
    {
        public CookieContainer GetCookies(string url)
        {
            var cookieContainer = new CookieContainer();
            AddCookies(url, cookieContainer);
            return cookieContainer;
        }

        public CookieContainer GetCookies(params string[] urls)
        {
            var cookieContainer = new CookieContainer();
            foreach (var url in urls)
            {
                AddCookies(url, cookieContainer);
            }
            return cookieContainer;
        }

        private static void AddCookies(string url, CookieContainer cookieContainer)
        {
            var nsUrl = new NSUrl(url);
            foreach (var cookie in NSHttpCookieStorage.SharedStorage.CookiesForUrl(nsUrl))
            {
                cookieContainer.Add(new Cookie
                {
                    Comment = cookie.Comment,
                    Domain = cookie.Domain,
                    HttpOnly = cookie.IsHttpOnly,
                    Name = cookie.Name,
                    Path = cookie.Path,
                    Secure = cookie.IsSecure,
                    Value = cookie.Value,
                    Version = Convert.ToInt32(cookie.Version),
                    // TODO expires? / expired?
                });
            }
        }
    }
}
