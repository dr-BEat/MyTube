﻿using System;
using System.IO;
using System.Threading.Tasks;
using MyTube.iOS.Services;
using MyTube.Services;
using Xamarin.Forms;

[assembly: Dependency(typeof(FileHelper))]
namespace MyTube.iOS.Services
{
    public class FileHelper : IFileHelper
    {
        public string DefaultVideosFolderPath => Environment.GetFolderPath(Environment.SpecialFolder.MyVideos);

        public string LocalFolderPath
        {
            get
            {
                var docFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                var libFolder = Path.Combine(docFolder, "..", "Library", "Databases");

                if (!Directory.Exists(libFolder))
                {
                    Directory.CreateDirectory(libFolder);
                }
                return libFolder;
            }
        }

        public string GetLocalFilePath(string filename)
        {
            return Path.Combine(LocalFolderPath, filename);
        }

        public void Delete(string filename)
        {
            File.Delete(filename);
        }

        public async Task<string> ReadAllTextAsync(string filename)
        {
            using (var filestream = File.OpenRead(filename))
            using (var reader = new StreamReader(filestream))
            {
                return await reader.ReadToEndAsync();
            }
        }

        public async Task WriteAllTextAsync(string filename, string contents)
        {
            using (var filestream = File.OpenWrite(filename))
            using (var writer = new StreamWriter(filestream))
            {
                await writer.WriteAsync(contents);
            }
        }
    }
}
