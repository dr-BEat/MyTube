﻿using System;
using System.Linq;
using System.Net;
using Windows.Web.Http.Filters;
using MyTube.Services;
using MyTube.UWP.Services;
using Xamarin.Forms;

[assembly: Dependency(typeof(CookieManager))]
namespace MyTube.UWP.Services
{
    class CookieManager : ICookieManager
    {
        public CookieContainer GetCookies(string url)
        {
            var cookieContainer = new CookieContainer();
            AddCookies(url, cookieContainer);
            return cookieContainer;
        }

        public CookieContainer GetCookies(params string[] urls)
        {
            var cookieContainer = new CookieContainer();
            foreach (var url in urls)
            {
                AddCookies(url, cookieContainer);
            }
            return cookieContainer;
        }

        private static void AddCookies(string url, CookieContainer cookieContainer)
        {
            var uri = new Uri(url);
            var httpBaseProtocolFilter = new HttpBaseProtocolFilter();
            var cookieManager = httpBaseProtocolFilter.CookieManager;
            var cookieCollection = cookieManager.GetCookies(uri);
            
            foreach (var cookie in cookieCollection)
            {
                cookieContainer.Add(uri, new Cookie
                {
                    Name = cookie.Name,
                    Domain = cookie.Domain,
                    Path = cookie.Path,
                    Secure = cookie.Secure,
                    HttpOnly = cookie.HttpOnly,
                    Value = cookie.Value,
                    //TODO: Handle expires
                });
            }
        }


    }
}
