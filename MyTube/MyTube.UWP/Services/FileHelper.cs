﻿using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Windows.Storage;
using MyTube.Services;
using MyTube.UWP.Services;
using Xamarin.Forms;

[assembly: Dependency(typeof(FileHelper))]
namespace MyTube.UWP.Services
{
    public class FileHelper : IFileHelper
    {
        public string DefaultVideosFolderPath => Path.Combine(KnownFolders.RemovableDevices.GetFoldersAsync().GetResults().FirstOrDefault().Path, "Videos");

        public string LocalFolderPath => ApplicationData.Current.LocalFolder.Path;

        public string GetLocalFilePath(string filename)
        {
            return Path.Combine(LocalFolderPath, filename);
        }

        public void Delete(string filename)
        {
            File.Delete(filename);
        }

        public async Task<string> ReadAllTextAsync(string filename)
        {
            using (var filestream = File.OpenRead(filename))
            using (var reader = new StreamReader(filestream))
            {
                return await reader.ReadToEndAsync();
            }
        }

        public async Task WriteAllTextAsync(string filename, string contents)
        {
            using (var filestream = File.OpenWrite(filename))
            using (var writer = new StreamWriter(filestream))
            {
                await writer.WriteAsync(contents);
            }
        }
    }
}
