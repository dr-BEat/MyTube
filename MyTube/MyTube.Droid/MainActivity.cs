﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Android.Util;
using Android.Widget;

namespace MyTube.Droid
{
    [Activity(Label = "MyTube", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        private const int PermissionRequestCode = 1;

        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);
            LoadApplication(new App());

            VerifyPermission(Android.Manifest.Permission.WriteExternalStorage);
            VerifyPermission(Android.Manifest.Permission.ReadExternalStorage);
            VerifyPermission(Android.Manifest.Permission.Internet);
        }

        private void VerifyPermission(string permission)
        {
            if (!CheckPermission(permission))
            {
                RequestPermission(permission);
            }
        }

        private bool CheckPermission(string permission)
        {
            return ContextCompat.CheckSelfPermission(this, permission) == Permission.Granted;
        }

        private void RequestPermission(string permission)
        {
            if (ActivityCompat.ShouldShowRequestPermissionRationale(this, permission))
            {
                Toast.MakeText(this, "Write External Storage permission allows us to do store images. Please allow this permission in App Settings.", ToastLength.Long).Show();
            }
            else
            {
                ActivityCompat.RequestPermissions(this, new[] { permission }, PermissionRequestCode);
            }
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
        {
            if (requestCode != PermissionRequestCode)
                return;
            for (var i = 0; i < permissions.Length; i++)
            {
                Log.Info("value", $"Permission {permissions[i]} {(grantResults[i] == Permission.Granted ? "Granted" : "Denied")}.");
            }
        }
    }
}

