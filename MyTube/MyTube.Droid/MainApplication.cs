using System;
using Android.App;
using Android.Runtime;

namespace MyTube.Droid
{
    [Application]
    public class MainApplication : Application
    {
        public MainApplication()
        {
        }

        public MainApplication(IntPtr javaReference, JniHandleOwnership transer)
          : base(javaReference, transer)
        {
        }
    }
}