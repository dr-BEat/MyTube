using System;
using System.Linq;
using System.Net;
using MyTube.Droid.Services;
using MyTube.Services;
using Xamarin.Forms;

[assembly: Dependency(typeof(CookieManager))]
namespace MyTube.Droid.Services
{
    class CookieManager : ICookieManager
    {
        public CookieContainer GetCookies(string url)
        {
            var cookieContainer = new CookieContainer();
            AddCookies(url, cookieContainer);
            return cookieContainer;
        }

        public CookieContainer GetCookies(params string[] urls)
        {
            var cookieContainer = new CookieContainer();
            foreach (var url in urls)
            {
                AddCookies(url, cookieContainer);
            }
            return cookieContainer;
        }

        private static void AddCookies(string url, CookieContainer cookieContainer)
        {
            // .GetCookie returns ALL cookies related to the URL as a single, long
            // string which we have to split and parse
            var allCookiesForUrl = Android.Webkit.CookieManager.Instance.GetCookie(url);
            var uri = new Uri(url);
            foreach (var cookie in allCookiesForUrl?.Split(';') ?? Enumerable.Empty<string>())
            {
                cookieContainer.SetCookies(uri, cookie.Trim());
            }
        }
    }
}