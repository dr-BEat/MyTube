using System.IO;
using System.Threading.Tasks;
using MyTube.Droid.Services;
using MyTube.Services;
using Xamarin.Forms;
using Environment = System.Environment;

[assembly: Dependency(typeof(FileHelper))]
namespace MyTube.Droid.Services
{
    public class FileHelper : IFileHelper
    {
        public string DefaultVideosFolderPath => Path.Combine(Android.OS.Environment.ExternalStorageDirectory.AbsolutePath, "Videos");
        public string LocalFolderPath => Environment.GetFolderPath(Environment.SpecialFolder.Personal);

        public string GetLocalFilePath(string filename)
        {
            return Path.Combine(LocalFolderPath, filename);
        }

        public void Delete(string filename)
        {
            File.Delete(filename);
        }

        public async Task<string> ReadAllTextAsync(string filename)
        {
            using (var filestream = File.OpenRead(filename))
            using (var reader = new StreamReader(filestream))
            {
                return await reader.ReadToEndAsync();
            }
        }

        public async Task WriteAllTextAsync(string filename, string contents)
        {
            using (var filestream = new FileStream(filename, FileMode.Create))
            using (var writer = new StreamWriter(filestream))
            {
                await writer.WriteAsync(contents);
            }
        }
    }
}