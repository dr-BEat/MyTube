using System;
using Android.Content;
using MyTube.Droid.Services;
using MyTube.Services;
using Xamarin.Forms;


[assembly: Dependency(typeof(PlatformServicesExtended))]
namespace MyTube.Droid.Services
{
    class PlatformServicesExtended : IPlatformServicesExtended
    {
        public void OpenUri(Uri uri, string mimeType)
        {
            var intent = new Intent(Intent.ActionView);
            intent.SetDataAndType(Android.Net.Uri.Parse(uri.ToString()), mimeType);
            Android.App.Application.Context.StartActivity(intent);
        }
    }
}