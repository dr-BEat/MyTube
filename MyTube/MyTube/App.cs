﻿using MyTube.Views;
using Xamarin.Forms;

namespace MyTube
{
    public class App : Application
    {
        public App()
        {
            MainPage = new MainPage
            {
                BindingContext = new ViewModels.MyTube()
            };
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
