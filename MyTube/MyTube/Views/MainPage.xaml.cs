﻿using System.ComponentModel;
using System.Globalization;
using System.Linq;
using MyTube.Helpers;
using MyTube.ViewModels;
using Xamarin.Forms;

namespace MyTube.Views
{
    public partial class MainPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        protected override void OnBindingContextChanged()
        {
            var context = BindingContext as ViewModels.MyTube;
            //The binding process resets the selectedPage property to null, we fix this here
            var seletedPage = context?.SelectedPage;
            base.OnBindingContextChanged();
            if (context != null)
            {
                context.SelectedPage = seletedPage;
                context.PropertyChanged += ContextOnPropertyChanged;
            }
            RefreshDetailPage();
        }
        
        private void RefreshDetailPage()
        {
            var pageItem = (BindingContext as ViewModels.MyTube)?.SelectedPage;
            //Check if we already show the page
            var currentPage = NavigationPage.Navigation.NavigationStack.LastOrDefault();
            if (currentPage?.BindingContext == pageItem)
                return; //Nothing to do the page is already shown

            var page = (Page)PageDataTemplateSelector.Convert(pageItem, typeof(Page), null, CultureInfo.CurrentCulture);
            NavigationPage.PushAsync(page).FireAndForget();
        }

        private void ContextOnPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            if (args.PropertyName == nameof(ViewModels.MyTube.SelectedPage))
            {
                RefreshDetailPage();
            }
        }
        
        protected override bool OnBackButtonPressed()
        {
            //Block the user from closing the application by pressing back
            if (NavigationPage.Navigation.NavigationStack.Count <= 1)
                return true;
            return base.OnBackButtonPressed();
        }

        private void NavigationPage_OnPopped(object sender, NavigationEventArgs e)
        {
            //Propagate the changed page back into our viewmodel
            var context = (ViewModels.MyTube)BindingContext;
            var currentPage = NavigationPage.Navigation.NavigationStack.Last();
            context.SelectedPage = (AbstractPageItem)currentPage.BindingContext;
        }
    }

    public class DetailAttachedProperties
    {
        public static readonly BindableProperty AttachedDetailProperty =
                            BindableProperty.CreateAttached("AttachedDetail", typeof(Page), typeof(DetailAttachedProperties), null);

        public static Page GetAttachedDetail(BindableObject view)
        {
            return (Page)view.GetValue(AttachedDetailProperty);
        }

        public static void SetAttachedDetail(BindableObject view, Page value)
        {
            view.SetValue(AttachedDetailProperty, value);
        }
    }
}
