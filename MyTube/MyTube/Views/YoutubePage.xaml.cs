﻿using System.Threading.Tasks;
using Xamarin.Forms;

namespace MyTube.Views
{
    public interface IWebViewAccessor
    {
        Task<string> GetCurrentUrlAsync();
    }

    public partial class YoutubePage : IWebViewAccessor
    {
        private const string CallbackSchema = "myapp://";
        
        public YoutubePage()
        {
            InitializeComponent();
        }

        protected override bool OnBackButtonPressed()
        {
            if (WebView.CanGoBack)
            {
                WebView.GoBack();
                return true;
            }
            return base.OnBackButtonPressed();
        }
        
        private TaskCompletionSource<string> _currentCompletionSource;

        private Task<string> RunInWebViewAsync(string script)
        {
            var completionSource = new TaskCompletionSource<string>();
            _currentCompletionSource = completionSource;
            WebView.Eval(script);
            return completionSource.Task;
        }

        public Task<string> GetCurrentUrlAsync()
        {
            return RunInWebViewAsync($"window.location.href = \"{CallbackSchema}\" + window.location.href;");
        }
        
        private void WebView_OnNavigating(object sender, WebNavigatingEventArgs e)
        {
            if (!e.Url.StartsWith(CallbackSchema))
                return;
            var completionSource = _currentCompletionSource;
            if (completionSource != null)
            {
                _currentCompletionSource = null;
                completionSource.SetResult(e.Url.Substring(CallbackSchema.Length));
            }
                
            e.Cancel = true;
        }
    }
}
