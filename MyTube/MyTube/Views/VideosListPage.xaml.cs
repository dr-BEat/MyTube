﻿using System;
using MyTube.Models;
using MyTube.ViewModels;
using Xamarin.Forms;

namespace MyTube.Views
{
    public partial class VideosListPage : ContentPage
    {
        public VideosListPage()
        {
            InitializeComponent();
        }

        private async void Button_OnClicked(object sender, EventArgs e)
        {
            var videoItem = (sender as BindableObject)?.BindingContext as Video;
            var videosList = BindingContext as VideosList;
            if(videoItem == null || videosList == null)
                return;
            var answer = await DisplayActionSheet("Menu", "Cancel", "Delete", "Play", "Remove Entry", "Delete and Remove from Playlist");
            switch (answer)
            {
                case "Delete":
                    videosList.DeleteVideoCommand.Execute(videoItem);
                    break;
                case "Play":
                    videosList.PlayVideoCommand.Execute(videoItem);
                    break;
                case "Remove Entry":
                    videosList.RemoveVideoEntryCommand.Execute(videoItem);
                    break;
                case "Delete and Remove from Playlist":
                    videosList.DeleteVideoAndRemoveFromPlaylistCommand.Execute(videoItem);
                    break;
                case "Cancel":
                default:
                    break;
            }
        }
    }


}
