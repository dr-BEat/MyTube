﻿using System;
using System.Globalization;
using System.Runtime.CompilerServices;
using MyTube.ViewModels;
using Xamarin.Forms;

namespace MyTube.Views.Converters
{
    public class PageProvider : IValueConverter
    {
        private readonly ConditionalWeakTable<object, object> _cache = new ConditionalWeakTable<object, object>();

        public DataTemplate SettingsTemplate { get; set; }
        public DataTemplate VideosListTemplate { get; set; }
        public DataTemplate YoutubeTemplate { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            object view = null;
            if (value != null && !_cache.TryGetValue(value, out view))
            {
                DataTemplate template = null;
                if (value is Settings)
                    template = SettingsTemplate;
                if (value is VideosList)
                    template = VideosListTemplate;
                if (value is Youtube)
                    template = YoutubeTemplate;

                if (template != null)
                {
                    view = template.CreateContent();
                    if (view is BindableObject bindable)
                    {
                        bindable.BindingContext = value;
                    }
                    _cache.Add(value, view);
                }
            }
            return view ?? new ContentPage() {Title = "Converter"};
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
