﻿using System;
using System.Globalization;
using MyTube.Models;
using Xamarin.Forms;

namespace MyTube.Views.Converters
{
    public class VideoThumbnailUrlProvider : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var videoId = (value as Video)?.VideoId ?? value as string;
            return videoId == null ? null : new Uri($"https://img.youtube.com/vi/{videoId}/default.jpg");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
