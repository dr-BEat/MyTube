using Plugin.Settings;
using Plugin.Settings.Abstractions;
using Xamarin.Forms;
using MyTube.Services;

namespace MyTube.Helpers
{
    /// <summary>
    /// This is the Settings static class that can be used in your Core solution or in any
    /// of your client applications. All settings are laid out the same exact way with getters
    /// and setters. 
    /// </summary>
    public static class Settings
    {
        private static ISettings AppSettings
        {
            get { return CrossSettings.Current; }
        }

        public static bool AllowMobileNetwork
        {
            get { return AppSettings.GetValueOrDefault(nameof(AllowMobileNetwork), false); }
            set { AppSettings.AddOrUpdateValue(nameof(AllowMobileNetwork), value); }
        }

        public static string VideoStorageFolder
        {
            get { return AppSettings.GetValueOrDefault(nameof(VideoStorageFolder), DependencyService.Get<IFileHelper>().DefaultVideosFolderPath); }
            set { AppSettings.AddOrUpdateValue(nameof(VideoStorageFolder), value); }
        }
    }
}