﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MyTube.Helpers
{
    public static class ExtensionMethods
    {
        public static async void FireAndForget(this Task task)
        {
            try
            {
                await task;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
        }

        public static void AddRange<T>(this ICollection<T> collection, IEnumerable<T> elements)
        {
            foreach (var element in elements)
            {
                collection.Add(element);
            }
        }

        public static string GetSafeFilename(this string filename, string replaceWith = null)
        {
            return string.Join(replaceWith ?? string.Empty, filename.Split(Path.GetInvalidFileNameChars()));
        }

        public static string FilePathToFileUrl(string filePath)
        {
            var uri = new StringBuilder();
            foreach (var c in filePath)
            {
                if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9') ||
                  c == '+' || c == '/' || c == ':' || c == '.' || c == '-' || c == '_' || c == '~' ||
                  c > '\xFF')
                {
                    uri.Append(c);
                }
                else if (c == '\\')
                {
                    uri.Append('/');
                }
                else
                {
                    uri.Append($"%{(int) c:X2}");
                }
            }
            if (uri.Length >= 2 && uri[0] == '/' && uri[1] == '/') // UNC path
                uri.Insert(0, "file:");
            else
            {
                if (uri.Length >= 1 && uri[0] != '/')
                    uri.Insert(0, '/');
                uri.Insert(0, "file://");
            }
            return uri.ToString();
        }
        
        public static async Task<SemaphoreReleaser> TakeLockAsync(this SemaphoreSlim semaphoreSlim)
        {
            await semaphoreSlim.WaitAsync();
            return new SemaphoreReleaser(semaphoreSlim);
        }

        public struct SemaphoreReleaser : IDisposable
        {
            private readonly SemaphoreSlim _semaphoreSlim;

            public SemaphoreReleaser(SemaphoreSlim semaphoreSlim)
            {
                _semaphoreSlim = semaphoreSlim;
            }
            
            public void Dispose()
            {
                _semaphoreSlim.Release();
            }
        }
    }
}
