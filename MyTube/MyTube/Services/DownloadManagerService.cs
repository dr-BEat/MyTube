﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using MyTube.Helpers;
using Plugin.DownloadManager;
using Plugin.DownloadManager.Abstractions;
using Strilanc.Value;
using YoutubeExplode;
using YoutubeExplode.Models;
using YoutubeExplode.Models.MediaStreams;
using YoutubeExplode.Services;


namespace MyTube.Services
{
    public class DownloadManagerService
    {
        private readonly ObservableCollection<IDownloadFile> _queue = new ObservableCollection<IDownloadFile>();
        public ReadOnlyObservableCollection<IDownloadFile> Queue { get; }

        public DownloadManagerService()
        {
            Queue = new ReadOnlyObservableCollection<IDownloadFile>(_queue);

            CrossDownloadManager.Current.PathNameForDownloadedFile = GetDownloadFilePath;

            CrossDownloadManager.Current.CollectionChanged += DownloadManager_OnCollectionChanged;
        }

        private void DownloadManager_OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs args)
        {
            switch (args.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    _queue.Add((IDownloadFile)args.NewItems[0]);
                    break;
                case NotifyCollectionChangedAction.Remove:
                    _queue.Remove((IDownloadFile)args.OldItems[0]);
                    break;
                case NotifyCollectionChangedAction.Move:
                    break;
                case NotifyCollectionChangedAction.Reset:
                    _queue.Clear();
                    break;
                case NotifyCollectionChangedAction.Replace:
                    throw new NotSupportedException();
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public async Task<May<string>> GetVideoStreamUrlAsync(string videoId)
        {
            var video = await LoadVideoAsync(videoId).ConfigureAwait(false);
            return video.Select(v => v.Item2.Url);
        }

        public async Task<May<IDownloadFile>> StartVideoDownloadAsync(string videoId)
        {
            var video = await LoadVideoAsync(videoId).ConfigureAwait(false);
            return video.Select(ScheduleDownload);
        }

        public async Task<List<IDownloadFile>> StartDownloadsAsync(IEnumerable<string> videoIds)
        {
            var downloadFiles = await Task.WhenAll(videoIds.Select(StartVideoDownloadAsync));
            return downloadFiles.WhereHasValue().ToList();
        }

        public async Task<List<IDownloadFile>> StartPlaylistDownloadAsync(string playlistId, IHttpService httpService = null)
        {
            var client = new YoutubeClient(httpService ?? HttpServiceExtensions.CreateHttpService());
            var playlist = await client.GetPlaylistAsync(playlistId);
            var videoTasks = playlist.Videos.Select(v => LoadVideoAsync(v.Id));
            var videos = await Task.WhenAll(videoTasks).ConfigureAwait(false);
            return videos.WhereHasValue()
                                .Select(ScheduleDownload)
                                .ToList();
        }

        private static async Task<May<Tuple<Video, MediaStreamInfo>>> LoadVideoAsync(string videoId)
        {
            var client = new YoutubeClient();
            var video = await client.GetVideoAsync(videoId);
            var info = await client.GetVideoMediaStreamInfosAsync(videoId).ConfigureAwait(false);
            var streams = info.Muxed.Where(s => s.VideoQuality <= VideoQuality.High720) //Remove all high resolution videos
                                    .OrderByDescending(s => s.VideoQuality)
                                    .ThenByDescending(s => s.Container == Container.Mp4) //Prefer MP4 over other formats
                                    .ThenByDescending(v => v.Container == Container.WebM); //then WebM over other formats

            return streams.MayFirst().Select(s => Tuple.Create(video, (MediaStreamInfo)s));
        }

        private static IDownloadFile ScheduleDownload(Tuple<Video, MediaStreamInfo> video)
        {
            var downloadUrl = video.Item2.Url + "#" + WebUtility.UrlEncode((video.Item1.Title + "." + video.Item2.Container.GetFileExtension()).GetSafeFilename());
            var downloadFile = CrossDownloadManager.Current.CreateDownloadFile(downloadUrl);
            CrossDownloadManager.Current.Start(downloadFile, Settings.AllowMobileNetwork);
            return downloadFile;
        }
        
        public string GetDownloadFilePath(IDownloadFile downloadFile)
        {
            var fileName = WebUtility.UrlDecode(new Uri(downloadFile.Url).Fragment.Substring(1));
            var path = Path.Combine(Settings.VideoStorageFolder, fileName);
            return path;
        }

        public void CancelDownload(IDownloadFile downloadFile)
        {
            CrossDownloadManager.Current.Abort(downloadFile);
        }
    }
}
