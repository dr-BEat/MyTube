﻿using System.Net;

namespace MyTube.Services
{
    public interface ICookieManager
    {
        CookieContainer GetCookies(string url);
        CookieContainer GetCookies(params string[] urls);
    }
}
