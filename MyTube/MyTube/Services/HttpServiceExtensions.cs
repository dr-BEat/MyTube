﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using ModernHttpClient;
using Xamarin.Forms;
using YoutubeExplode.Services;

namespace MyTube.Services
{
    public static class HttpServiceExtensions
    {
        public static HttpService CreateHttpService()
        {
            return new HttpService(CreateHttpClient());
        }

        public static HttpService CreateAuthenticatedHttpService(params string[] urls)
        {
            return new HttpService(CreateHttpClient(DependencyService.Get<ICookieManager>().GetCookies(urls)));
        }
        
        public static HttpClient CreateHttpClient(CookieContainer cookieContainer = null)
        {
            var httpClientHandler = new NativeMessageHandler();
            if (httpClientHandler.SupportsAutomaticDecompression)
                httpClientHandler.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

            httpClientHandler.UseCookies = cookieContainer != null;
            httpClientHandler.CookieContainer = cookieContainer;

            var httpClient = new HttpClient(httpClientHandler);
            httpClient.DefaultRequestHeaders.Add("User-Agent", "YoutubeExplode (github.com/Tyrrrz/YoutubeExplode)");
            httpClient.DefaultRequestHeaders.Add("Connection", "Close");
            return httpClient;
        }
        
        public static async Task<string> PostAsync(this IHttpService httpService, string url, IEnumerable<KeyValuePair<string, string>> nameValuePairs)
        {
            if (url == null)
                throw new ArgumentNullException(nameof(url));
            try
            {
                using (var request = new HttpRequestMessage(HttpMethod.Post, url)
                {
                    Content = new FormUrlEncodedContent(nameValuePairs)
                })
                using (var response = await httpService.PerformRequestAsync(request).ConfigureAwait(false))
                {
                    response.EnsureSuccessStatusCode();
                    return await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                }
            }
            catch
            {
                return null;
            }
        }
    }
}
