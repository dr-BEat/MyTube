﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyTube.Services
{
    public interface IPlatformServicesExtended
    {
        void OpenUri(Uri uri, string mimeType);
    }
}
