﻿using System.Threading.Tasks;

namespace MyTube.Services
{
    public interface IFileHelper
    {
        string DefaultVideosFolderPath { get; }
        string LocalFolderPath { get; }
        string GetLocalFilePath(string filename);
        void Delete(string filename);
        Task<string> ReadAllTextAsync(string filename);
        Task WriteAllTextAsync(string filename, string contents);
    }
}
