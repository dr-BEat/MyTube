﻿using System.Runtime.Serialization;
using MyTube.ViewModels;

namespace MyTube.Models
{
    public enum DownloadState
    {
        NotStarted,
        Downloading,
        Paused,
        Canceled,
        Failed,
        Finished
    }

    [DataContract]
    public class Video:AbstractPropertyChangedNotifier
    {
        private long _fileSize;
        private long? _downloadedFileSize;
        private DownloadState _downloadState;
        
        [DataMember]
        public string VideoId { get; set; }

        [DataMember]
        public string PlaylistId { get; set; }

        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string FileName { get; set; }
        [DataMember]
        public bool New { get; set; }

        [DataMember]
        public DownloadState DownloadState
        {
            get => _downloadState;
            set
            {
                if (_downloadState == value)
                    return;
                _downloadState = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(DownloadProgress));
            }
        }

        [DataMember]
        public long FileSize
        {
            get => _fileSize;
            set
            {
                if(_fileSize == value)
                    return;
                _fileSize = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(DownloadProgress));
            }
        }
        
        public long DownloadedFileSize
        {
            get => _downloadedFileSize ?? FileSize;
            set
            {
                if (_downloadedFileSize == value)
                    return;
                _downloadedFileSize = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(DownloadProgress));
            }
        }

        public double DownloadProgress
        {
            get
            {
                switch (DownloadState)
                {
                    case DownloadState.Downloading:
                    case DownloadState.Paused:
                    {
                        if (FileSize == 0)
                            return 0.0;
                        return (double)DownloadedFileSize / FileSize;
                    }
                    case DownloadState.Finished:
                        return 1.0;
                    case DownloadState.Canceled:
                    case DownloadState.NotStarted:
                    case DownloadState.Failed:
                    default:
                        return 0.0;
                }
            }
        }
    }
}
