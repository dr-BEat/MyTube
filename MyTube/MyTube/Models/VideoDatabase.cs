﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using MyTube.Helpers;
using MyTube.Services;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace MyTube.Models
{
    internal class VideoDatabase
    {
        private readonly SemaphoreSlim _semaphoreSlim = new SemaphoreSlim(1, 1);
        private readonly string _dbPath;
        private List<Video> _videos;

        private VideoDatabase(string dbPath)
        {
            _dbPath = dbPath;
        }

        private async Task Initialize()
        {
            var fileHelper = DependencyService.Get<IFileHelper>();
            try
            {
                var json = await fileHelper.ReadAllTextAsync(_dbPath);
                _videos = JsonConvert.DeserializeObject<List<Video>>(json);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                _videos = new List<Video>();
            }
        }

        public static async Task<VideoDatabase> Initialize(string dbPath)
        {
            var database = new VideoDatabase(dbPath);
            await database.Initialize();
            return database;
        }

        public async Task<List<Video>> GetVideosAsync()
        {
            using (await _semaphoreSlim.TakeLockAsync())
            {
                return _videos;
            }
        }
        
        public async Task AddVideoAsync(Video video)
        {
            using (await _semaphoreSlim.TakeLockAsync())
            {
                if (!_videos.Contains(video))
                {
                    _videos.Add(video);
                }
                var fileHelper = DependencyService.Get<IFileHelper>();
                var json = JsonConvert.SerializeObject(_videos);
                await fileHelper.WriteAllTextAsync(_dbPath, json);
            }
        }

        public async Task DeleteVideoAsync(Video video)
        {
            using (await _semaphoreSlim.TakeLockAsync())
            {
                _videos.Remove(video);
                var fileHelper = DependencyService.Get<IFileHelper>();
                var json = JsonConvert.SerializeObject(_videos);
                await fileHelper.WriteAllTextAsync(_dbPath, json);
            }
        }
    }
}
