﻿namespace MyTube.ViewModels
{
    public class AbstractPageItem : AbstractPropertyChangedNotifier
    {
        private string _title;
        private string _detail;
        private string _imageSource;

        public string Title
        {
            get => _title;
            set
            {
                if(_title == value)
                    return;
                
                _title = value;
                OnPropertyChanged();
            }
        }

        public string Detail
        {
            get => _detail;
            set
            {
                if (_detail == value)
                    return;

                _detail = value;
                OnPropertyChanged();
            }
        }

        public string ImageSource
        {
            get => _imageSource;
            set
            {
                if (_imageSource == value)
                    return;

                _imageSource = value;
                OnPropertyChanged();
            }
        }
    }
}
