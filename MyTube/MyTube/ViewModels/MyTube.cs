﻿using System.Collections.Generic;
using System.Linq;

namespace MyTube.ViewModels
{
    public class MyTube : AbstractPropertyChangedNotifier
    {
        private AbstractPageItem _selectedPage;
        private bool _isPresented;

        public IReadOnlyList<AbstractPageItem> Pages { get; } = new AbstractPageItem[]
        {
            new VideosList(),
            new Youtube(), 
            new Settings()
        };

        public AbstractPageItem SelectedPage
        {
            get => _selectedPage;
            set
            {
                if (_selectedPage == value)
                    return;

                _selectedPage = value;
                OnPropertyChanged();

                IsPresented = false;
            }
        }

        public bool IsPresented
        {
            get => _isPresented;
            set
            {
                if (_isPresented == value)
                    return;

                _isPresented = value;
                OnPropertyChanged();
            }
        }

        public MyTube()
        {
            var videoList = Pages.OfType<VideosList>().First();
            Pages.OfType<Youtube>().First().VideosList = videoList;
            SelectedPage = videoList;
        }
    }
}
