﻿namespace MyTube.ViewModels
{
    public class Settings : AbstractPageItem
    {
        public bool AllowMobileNetwork
        {
            get => Helpers.Settings.AllowMobileNetwork;
            set
            {
                if (Helpers.Settings.AllowMobileNetwork == value)
                    return;
                Helpers.Settings.AllowMobileNetwork = value;
                OnPropertyChanged();
            }
        }

        public string VideoStorageFolder
        {
            get => Helpers.Settings.VideoStorageFolder;
            set
            {
                if (Helpers.Settings.VideoStorageFolder == value)
                    return;
                Helpers.Settings.VideoStorageFolder = value;
                OnPropertyChanged();
            }
        }

        public Settings()
        {
            Title = "Settings";
            Detail = "You can change the settings here.";
        }
    }
}
