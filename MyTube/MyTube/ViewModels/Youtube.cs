﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using MyTube.Helpers;
using MyTube.Services;
using MyTube.Views;
using Strilanc.Value;
using Xamarin.Forms;
using YoutubeExplode;

namespace MyTube.ViewModels
{
    public class Youtube : AbstractPageItem
    {
        private bool _isBusy;
        public ICommand DownloadCommand { get; }
        public ICommand StreamCommand { get; }
        public ICommand DownloadWatchLaterCommand { get; }

        public bool IsBusy
        {
            get => _isBusy;
            set
            {
                if(_isBusy == value)
                    return;
                _isBusy = value;
                OnPropertyChanged();
            }
        }

        public VideosList VideosList { get; set; }

        public Youtube()
        {
            Title = "Youtube";

            DownloadCommand = new Command<IWebViewAccessor>(a => DownloadAsync(a).FireAndForget());
            StreamCommand = new Command<IWebViewAccessor>(a => StreamAsync(a).FireAndForget());
            DownloadWatchLaterCommand = new Command<IWebViewAccessor>(a => DownloadWatchLaterAsync(a).FireAndForget());
        }

        private Task DownloadWatchLaterAsync(IWebViewAccessor webViewAccessor)
        {
            return DownloadAsync(webViewAccessor, "https://www.youtube.com/playlist?list=WL");
        }

        private async Task DownloadAsync(IWebViewAccessor webViewAccessor, string url = null)
        {
            IsBusy = true;
            try
            {
                if (url == null)
                {
                    url = await webViewAccessor.GetCurrentUrlAsync();
                }
                if (YoutubeClient.TryParseVideoId(url, out var id))
                {
                    await VideosList.StartVideoDownloadAsync(id);
                }
                else if (YoutubeClient.TryParsePlaylistId(url, out id))
                {
                    await VideosList.StartPlaylistDownloadAsync(id);
                }
            }
            finally
            {
                IsBusy = false;
            }
        }

        private async Task StreamAsync(IWebViewAccessor webViewAccessor)
        {
            IsBusy = true;
            try
            {
                var url = await webViewAccessor.GetCurrentUrlAsync();
                if (YoutubeClient.TryParseVideoId(url, out var videoId))
                {
                    var streamUrl = await DependencyService.Get<DownloadManagerService>()
                                                           .GetVideoStreamUrlAsync(videoId);
                    streamUrl.IfHasValueThenDo(OpenMediaStream);
                }
            }
            finally
            {
                IsBusy = false;
            }
        }
        
        private static void OpenMediaStream(string url)
        {
            try
            {
                DependencyService.Get<IPlatformServicesExtended>().OpenUri(new Uri(url), "video/*");
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }
    }
}
