﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MyTube.Helpers;
using MyTube.Models;
using MyTube.Services;
using Newtonsoft.Json.Linq;
using Plugin.DownloadManager.Abstractions;
using Strilanc.Value;
using Xamarin.Forms;
using YoutubeExplode.Services;
using YoutubeExplode;

namespace MyTube.ViewModels
{
    public class VideosList : AbstractPageItem
    {
        private readonly Dictionary<IDownloadFile, Video> _currentDownloadItems = new Dictionary<IDownloadFile, Video>();
        private VideoDatabase _database;

        public DownloadManagerService DownloadManagerService { get; } = new DownloadManagerService();

        public ObservableCollection<Video> VideoItems { get; } = new ObservableCollection<Video>();

        public Command<Video> PlayVideoCommand { get; }
        public Command<Video> DeleteVideoCommand { get; }
        public Command<Video> RemoveVideoEntryCommand { get; }
        public Command DeleteVideoRemovedFromPlaylistCommand { get; }
        public Command<Video> DeleteVideoAndRemoveFromPlaylistCommand { get; }
        public Command DownloadWatchLaterCommand { get; }


        public VideosList()
        {
            Title = "VideosList";
            
            ((INotifyCollectionChanged)DownloadManagerService.Queue).CollectionChanged += DownloadQueue_OnCollectionChanged;

            PlayVideoCommand = new Command<Video>(PlayVideo, item => item != null);
            DeleteVideoCommand = new Command<Video>(item => RemoveVideo(item, true).FireAndForget(), item => item != null);
            RemoveVideoEntryCommand = new Command<Video>(item => RemoveVideo(item).FireAndForget(), item => item != null);
            DeleteVideoAndRemoveFromPlaylistCommand = new Command<Video>(item => RemoveVideo(item, true, true).FireAndForget(), item => item != null);
            DeleteVideoRemovedFromPlaylistCommand = new Command(() => RemoveVideoRemovedFromPlaylist().FireAndForget());
            DownloadWatchLaterCommand = new Command(() => StartPlaylistDownloadAsync("WL").FireAndForget());
            Initialize().FireAndForget();
        }
        
        private static void PlayVideo(Video item)
        {
            var uri = new Uri(ExtensionMethods.FilePathToFileUrl(item.FileName));
            try
            {
                DependencyService.Get<IPlatformServicesExtended>().OpenUri(uri, "video/*");
            }
            catch (Exception)
            {
                //Ignore!!!
            }
        }

        private static HttpService CreateHttpService()
        {
            return HttpServiceExtensions.CreateAuthenticatedHttpService("https://www.youtube.com", "https://m.youtube.com");
        }
        
        private async Task RemoveVideo(Video item, bool deleteFile = false, bool removeFromPlaylist = false)
        {
            var downloadFile = _currentDownloadItems.FirstOrDefault(kvp => kvp.Value == item).Key;
            if (downloadFile != null)
            {
                DownloadManagerService.CancelDownload(downloadFile);
            }
            await _database.DeleteVideoAsync(item);
            VideoItems.Remove(item);

            if (deleteFile)
            {
                DependencyService.Get<IFileHelper>().Delete(item.FileName);
            }

            if (removeFromPlaylist && !string.IsNullOrEmpty(item.PlaylistId))
            {
                using (var httpService = CreateHttpService())
                {
                    var videoSetIds = await GetVideoIdsInPlaylistAsync(item.PlaylistId, httpService);
                    var matchingData = videoSetIds.Videos.Where(t => t.VideoId == item.VideoId)
                                                  .MayFirst()
                                                  .Select(t => t.SetVideoId);
                    if(!matchingData.HasValue)
                        return;
                    var setVideoId = matchingData.ForceGetValue();

                    var data = new[]
                    {
                        new KeyValuePair<string, string>("playlist_id", item.PlaylistId),
                        new KeyValuePair<string, string>("set_video_id", setVideoId),
                        new KeyValuePair<string, string>("session_token", videoSetIds.SessionToken),
                    };
                    var result = await httpService.PostAsync("https://www.youtube.com/playlist_edit_service_ajax/?action_remove_video=1", data);
                }
            }
        }
        
        private async Task RemoveVideoRemovedFromPlaylist()
        {
            using (var httpService = CreateHttpService())
            {
                var client = new YoutubeClient(httpService);
                var playlistIdsTasks = VideoItems.Select(v => v.PlaylistId)
                    .Where(id => !string.IsNullOrEmpty(id))
                    .Distinct()
                    .Select(async id => (Id:id, Info:await client.GetPlaylistAsync(id)));
                var playlistIds = await Task.WhenAll(playlistIdsTasks);
                var playlistInfos = playlistIds.ToDictionary(t => t.Id, t => t.Info);

                foreach (var video in VideoItems.ToList())
                {
                    if (!string.IsNullOrEmpty(video.PlaylistId) &&
                        playlistInfos.TryGetValue(video.PlaylistId, out var playlistInfo) &&
                        playlistInfo.Videos.All(v => v.Id != video.VideoId))
                    {
                        await RemoveVideo(video, true);
                    }
                }
            }
        }

        private void DownloadQueue_OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs args)
        {
            if (args.Action != NotifyCollectionChangedAction.Remove)
                return;

            var downloadFile = args.OldItems.Cast<IDownloadFile>().Single();
            var video = _currentDownloadItems[downloadFile];
            _currentDownloadItems.Remove(downloadFile);
            downloadFile.PropertyChanged -= DownloadFile_OnPropertyChanged;

            _database.AddVideoAsync(video).FireAndForget();
        }

        private void DownloadFile_OnPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            var downloadFile = (IDownloadFile) sender;
            var video = _currentDownloadItems[downloadFile];
            switch (args.PropertyName)
            {
                case nameof(IDownloadFile.Status):
                    video.DownloadState = ToDownloadState(downloadFile.Status);
                    break;
                case nameof(IDownloadFile.TotalBytesExpected):
                    video.FileSize = (long)downloadFile.TotalBytesExpected;
                    break;
                case nameof(IDownloadFile.TotalBytesWritten):
                    video.DownloadedFileSize = (long)downloadFile.TotalBytesWritten;
                    break;
            }
        }

        private static DownloadState ToDownloadState(DownloadFileStatus fileStatus)
        {
            switch (fileStatus)
            {
                case DownloadFileStatus.INITIALIZED:
                case DownloadFileStatus.PENDING:
                    return DownloadState.NotStarted;
                case DownloadFileStatus.RUNNING:
                    return DownloadState.Downloading;
                case DownloadFileStatus.PAUSED:
                    return DownloadState.Paused;
                case DownloadFileStatus.COMPLETED:
                    return DownloadState.Finished;
                case DownloadFileStatus.CANCELED:
                    return DownloadState.Canceled;
                case DownloadFileStatus.FAILED:
                    return DownloadState.Failed;
                default:
                    throw new ArgumentOutOfRangeException(nameof(fileStatus), fileStatus, null);
            }
        }

        private async Task Initialize()
        {
            _database = await VideoDatabase.Initialize(DependencyService.Get<IFileHelper>().GetLocalFilePath("Videos.db"));

            VideoItems.AddRange(await _database.GetVideosAsync());
        }

        public async Task<May<Video>> StartVideoDownloadAsync(string videoId, string playlistId = null)
        {
            var existingVideo = VideoItems.FirstOrDefault(v => v.VideoId == videoId);
            if (existingVideo != null)
                return existingVideo;
            var maybeDownloadFile = await DownloadManagerService.StartVideoDownloadAsync(videoId);
            if(!maybeDownloadFile.HasValue)
                return May.NoValue;
            var downloadFile = maybeDownloadFile.ForceGetValue();
            var fileName = DownloadManagerService.GetDownloadFilePath(downloadFile);
            var video = new Video
            {
                VideoId = videoId,
                PlaylistId = playlistId,
                FileName = fileName,
                Name = Path.GetFileNameWithoutExtension(fileName),
                New = true,
                DownloadState = ToDownloadState(downloadFile.Status),
                FileSize = (long)downloadFile.TotalBytesExpected,
            };
            VideoItems.Add(video);
            _currentDownloadItems.Add(downloadFile, video);
            downloadFile.PropertyChanged += DownloadFile_OnPropertyChanged;
            await _database.AddVideoAsync(video);
            return video;
        }

        public async Task<List<Video>> StartPlaylistDownloadAsync(string playlistId)
        {
            using (var httpService = CreateHttpService())
            {
                var client = new YoutubeClient(httpService);
                var playlist = await client.GetPlaylistAsync(playlistId);
                var videoTasks = playlist.Videos.Select(v => StartVideoDownloadAsync(v.Id, playlistId));
                var videos = await Task.WhenAll(videoTasks);
                return videos.WhereHasValue().ToList();
            }
        }
        
        public async Task<(List<(string VideoId, string SetVideoId)> Videos, string SessionToken)>
                     GetVideoIdsInPlaylistAsync(string playlistId, IHttpService requestService)
        {
            var videoIds = new List<(string VideoId, string SetVideoId)>();
            var baseUrl = $"https://m.youtube.com/playlist?list={playlistId}&ajax=1&tsp=1&app=m";
            var nextUrl = baseUrl;
            string sessionToken = null;
            while (nextUrl != null)
            {
                var responseContent = await requestService.GetStringAsync(nextUrl).ConfigureAwait(false);
                nextUrl = null;

                if(responseContent == null)
                    continue;

                var json = JObject.Parse(responseContent.Substring(4));
                if (json["result"].Value<string>() != "ok")
                    continue;

                var token = json["content"]?["manage_playlist_xsrf_token"]?.Value<string>();
                if (token != null)
                    sessionToken = token;

                //Normal or continuation content
                var content = json["content"]?["section_list"]?["contents"]?.First?["contents"]?.First ??
                              json["content"]?["continuation_contents"];

                if (content?["item_type"]?.Value<string>() != "playlist_video_list")
                    continue;

                foreach (var videoData in content["contents"])
                {
                    var videoId = videoData["video_id"]?.Value<string>();
                    var setVideoId = videoData["set_video_id"]?.Value<string>();

                    videoIds.Add((videoId, setVideoId));
                }
                
                var continuation = content["continuations"].First;
                if (continuation != null)
                {
                    nextUrl = baseUrl + "&action_continuation=1&ctoken=" + continuation["continuation"].Value<string>();
                }
            }
            return (videoIds, sessionToken);
        }

    }
}
